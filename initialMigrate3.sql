CREATE TABLE homeadhyaksa_gambarberita (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  photo varchar(100) NULL,
  berita_id int NOT NULL,
  FOREIGN KEY (berita_id) REFERENCES homeadhyaksa_berita (id)
);
