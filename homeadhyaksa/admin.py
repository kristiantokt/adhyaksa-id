from django.contrib import admin

# Register your models here.
from .models import Berita, Anggota, GambarBerita

class PhotoBeritaInline(admin.TabularInline):
    model = GambarBerita
    extra = 1

class BeritaAdmin(admin.ModelAdmin):
    inlines = [ PhotoBeritaInline, ]

admin.site.register(Berita, BeritaAdmin)
admin.site.register(Anggota)
