from django.db import models

# Create your models here.
class Berita(models.Model):
    judul = models.CharField(max_length=255)
    isi = models.TextField()
    dibuat_pada = models.DateTimeField(auto_now_add=True)
    ditulis_pada = models.DateTimeField(auto_now=False, auto_now_add=False)
    penulis = models.CharField(max_length=255)

    class Meta:
        ordering = ['-dibuat_pada']
        verbose_name = 'Berita'
        verbose_name_plural = 'Kumpulan Berita'

    def __str__(self):
        return self.judul

class Anggota(models.Model):
    nama_lengkap = models.CharField(max_length=255)
    photo_profile = models.ImageField(upload_to='PhotoProfile/')
    profile_info = models.TextField()
    position_in_company = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=12)
    email =  models.EmailField(max_length=50)
    priority = models.IntegerField(default=100)
    
    class Meta:
        ordering = ['priority', 'nama_lengkap']
        verbose_name = 'Anggota'
        verbose_name_plural = 'Seluruh Anggota'

    def __str__(self):
        return self.nama_lengkap 

class GambarBerita(models.Model):
    berita = models.ForeignKey(Berita, related_name='photos', on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='PhotoBerita/', null=True, blank=True)