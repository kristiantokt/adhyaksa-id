# homeadhyaksa.urls.py

from django.conf.urls import url
from homeadhyaksa import views

urlpatterns = [
    url(r'^$', views.HomePageView.as_view(), name='home'), # Notice the URL has been named
    url(r'^about', views.AboutUsPageView.as_view(), name='about_us'),
    url(r'^our-team', views.OurTeamListView.as_view(), name='our_team'),
    url(r'^news/$', views.NewsListView.as_view(), name='news'),
    url(r'^news/(?P<pk>[0-9]+)/(?P<judul>.*)/$', views.NewsDetailPageView.as_view(), name='news-details')
]