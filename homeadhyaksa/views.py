from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView, ListView, DetailView # Import TemplateView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Berita, Anggota, GambarBerita


# Create your views here.
class HomePageView(TemplateView):
    template_name = 'home.html'

class AboutUsPageView(TemplateView):
    template_name = 'about_us.html'

# class OurTeamPageView(TemplateView):
#     template_name = 'team.html'

class NewsListView(ListView):
    model = Berita
    template_name = 'news.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'berita2'  # Default: object_list
    paginate_by = 5
    queryset = Berita.objects.all()  # Default: Model.objects.all()

class NewsDetailPageView(DetailView):
    model = Berita
    template_name = 'news_detail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
    
class OurTeamListView(ListView):
    model = Anggota
    template_name = 'team.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'member2'  # Default: object_list
    queryset = Anggota.objects.all()  # Default: Model.objects.all()
