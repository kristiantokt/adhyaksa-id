CREATE TABLE homeadhyaksa_berita (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  judul varchar(255) NOT NULL,
  isi text NOT NULL,
  dibuat_pada datetime NOT NULL,
  ditulis_pada datetime NOT NULL,
  penulis varchar(255) NULL
);
