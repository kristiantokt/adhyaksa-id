CREATE TABLE homeadhyaksa_anggota (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nama_lengkap varchar(255) NOT NULL,
  nama_panggilan varchar(255) NULL,
  photo_profile varchar(100) NOT NULL,
  profile_info text NOT NULL,
  position_in_company varchar(255) NOT NULL,
  phone_number varchar(12) NOT NULL,
  email varchar(50) NOT NULL
);
